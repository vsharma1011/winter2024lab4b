import java.util.Scanner;
public class VirtualPetApp{
	public static void main (String[] args){
		Scanner getUserInput = new Scanner(System.in);
		// here we are bulding an array where we built each 'array space' with a flamingo
		Flamingo[] flamboyance = new Flamingo[1];
		for(int i = 0;i< flamboyance.length;i++){
			System.out.println("Do you think the flamingo is one one foot:(respond with a true or false)");
			boolean isonOnePaw =Boolean.parseBoolean(getUserInput.nextLine());
			System.out.println("Give a name to our cute flamingo");
			String hesName = getUserInput.nextLine();
			System.out.println("How many shrimps did your flamingo eat today");
			int numOfshrimps = Integer.parseInt(getUserInput.nextLine());
			flamboyance[i] = new Flamingo(isonOnePaw, hesName, numOfshrimps);
		}
		//Pre Setter Method
		System.out.println("Flamingo # " + flamboyance.length + " is on one foot: " + flamboyance[flamboyance.length -1].getIsOnOnePaw());
		System.out.println("Flamingo # " + flamboyance.length + " name is " + flamboyance[flamboyance.length -1].getHesName());
		System.out.println("Flamingo # " + flamboyance.length + " has eaten " + flamboyance[flamboyance.length -1].getNumOfShrimps() + " shrimp(s) before the set method");
		//Post Setter Method
		flamboyance[flamboyance.length -1].setNumOfShrimps(50000);
		System.out.println("Flamingo # " + flamboyance.length + " is on one foot: " + flamboyance[flamboyance.length -1].getIsOnOnePaw());
		System.out.println("Flamingo # " + flamboyance.length + " name is " + flamboyance[flamboyance.length -1].getHesName());
		System.out.println("Flamingo # " + flamboyance.length + " has eaten " + flamboyance[flamboyance.length -1].getNumOfShrimps() + " shrimp(s) after the set method");
	}
}


