public class Flamingo{
	private boolean isonOnePaw;
	private String hesName;
	private int numOfshrimps;
	
	//New constructor for flamingo
	public Flamingo(boolean isonOnePaw, String hesName, int numOfshrimps){
		this.isonOnePaw = isonOnePaw;
		this.hesName = hesName;
		this.numOfshrimps = numOfshrimps;
	}
	
    public void theColor(){
		if(this.numOfshrimps >= 50)
			System.out.println("The flamingo is now pink!");
		else{
			System.out.println("THE flamingo is still white!");
		}
	}
	public void isSleeping(){
		if(this.isonOnePaw)
			System.out.println("The flamingo is sleeping");
		else{
			System.out.println("the flamingo is on his 2 feet and awake!");
		}
	}
	//Set Methods
	public void setNumOfShrimps(int numOfshrimps){
		this.numOfshrimps = numOfshrimps;
	}
	//Get Methods
	public boolean getIsOnOnePaw(){
		return this.isonOnePaw;
	}
	public String getHesName(){
		return this.hesName;
	}
	public int getNumOfShrimps(){
		return this.numOfshrimps;
	}

}